<?php
    declare(strict_types=1);

    use Siler\Swoole;

    function validateInput(?string $parameter, string $fallbackValue) : string
    {
        return Swoole\request()->get[$parameter] ?? $fallbackValue;
    }

    return function () {
        $response = [
            'title' => validateInput('title', 'Title'),
            'description' => 'Description',
            'image' => 'Image not found',
            'url' => validateInput('url', 'https://pushnami.com'),
        ];
        Swoole\cors();
        Swoole\json($response);
    };