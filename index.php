<?php
    declare(strict_types=1);
    require_once 'vendor/autoload.php';

    use Siler\Route;
    use Siler\Swoole;

    $handler = function ($request) {
        Route\get('/', 'pages/home.php');
        Route\get('/preview', 'link/preview.php');

        Swoole\emit('Not Found', 404);
    };

    Swoole\http($handler)->start();