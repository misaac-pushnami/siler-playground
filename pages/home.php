<?php
    declare(strict_types=1);

    use Siler\Swoole;

    return function () {
        $name = Swoole\request()->get['name'] ?? 'Unknown Person';
        Swoole\emit("Heyo ${name}!");
    };